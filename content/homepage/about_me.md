---
title: "About (home)"
draft: false
author: "Daniel Liden"
title: Daniel Liden
avatar: images/portrait.png
role: 
organization:
  name: 
  url: 
bio: Data scientist and public policy researcher with training in Statistics and machine learning
weight: 1
widget:
  handler: about
  width:
  sidebar:
  background:
    color: secondary
---

Most recently, Daniel Liden was the Director of Data Analytics at the Guinn Center for Policy
Priorities in Las Vegas, Nevada. In this role, he was responsible for conducting data-driven policy
analysis and for supporting ongoing research and data analysis at the Guinn Center.
