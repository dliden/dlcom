---
title: "Publications"
draft: false
author: "Daniel Liden"
title: Publications
description:
toc: true
authors: [dan]
date: 2021-03-27T08:39:28-07:00
lastmod: 2021-03-27T08:39:28-07:00
draft: false
weight: 1
---

## Academic and Industry Publications {#academic-and-industry-publications}


### Research {#research}


#### 2020 {#2020}

-   [The Impact of COVID-19 on Communities of Color in Nevada](https://guinncenter.org/wp-content/uploads/2020/09/Guinn-Center-Impact-of-COVID-19-on-Communities-of-Color-in-Nevada.pdf)
-   [COVID-19 Affects Housing Security in Nevada](https://guinncenter.org/wp-content/uploads/2020/03/Guinn-Center-Housing-Insecurity-2020.pdf)
-   [COVID-19 and the Impact of Recession on the Economy and Housing in Nevada](https://guinncenter.org/wp-content/uploads/2020/05/Housing-and-Economy-in-Historical-Context-Policy-Brief.pdf)


#### 2019 {#2019}

-   [Federal Housing Assistance in Nevada](https://guinncenter.org/wp-content/uploads/2019/12/Guinn-Center-Housing-Federal-Aid-2019.pdf)
-   [How Do Target Population Sizes In Health Technology Assessments Impact Drug Price Changes?](https://www.cambridge.org/core/journals/international-journal-of-technology-assessment-in-health-care/article/pp50-how-do-target-population-sizes-in-health-technology-assessments-impact-drug-price-changes/9B8A0A7C0A83E49D46F5B67325C599B7) (Presented at Health Technology Assessment International meeting in 2019)


#### 2018 {#2018}

-   [How do HTA Decisions for New Disease Conditions Impact Time to Price Change?](https://www.valueinhealthjournal.com/article/S1098-3015(18)33657-X/fulltext) (Presented at ISPOR Europe 2018)
-   [Time to Price Change Following HTA Decisions](https://www.valueinhealthjournal.com/article/S1098-3015(18)33656-8/pdf) (Presented at ISPOR Europe 2018)


#### 2017 {#2017}

-   [Cancer Drugs Fund Allocation Under NICE: The First Six Months](https://www.cambridge.org/core/journals/international-journal-of-technology-assessment-in-health-care/article/vp22-cancer-drugs-fund-allocation-under-the-national-institute-for-health-and-care-excellence-nice-the-first-six-months/6DDAED65F0C4A381A3FC297E78BB2482) (Presented at HTAi Rome 2017)
-   [Trends In NICE Cancer Drugs Fund Reconsiderations](https://www.cambridge.org/core/journals/international-journal-of-technology-assessment-in-health-care/article/op03-trends-in-the-national-institute-for-health-and-care-excellence-nice-cancer-drugs-fund-reconsiderations/28C15DA3E0EBEF06ADF1DC08D6959BA4) (Presented at HTAi Rome 2017)


#### 2016 {#2016}

-   [Institute for Clinical and Economic Review (ICER) VS. Health Technology Assessment (HTA) Agencies: The Case of Multiple Myeloma and PCSK9S (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(16)32217-3/fulltext)
-   [Different Values In Cost-Effectiveness Research: Institute for Clinical and Economic Review (ICER) VS. The National Institute for Care Excellence (NICE) (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(16)32263-X/fulltext)


#### 2015 {#2015}

-   [A Comparison of G-BA's additional benefit score to NICE ICERs (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(15)00616-6/fulltext)
-   [Predictors of a positive Cancer Drug fund decision (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(15)01330-3/fulltext)
-   [Do evidence review groups bias nice decisions? (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(15)02189-0/fulltext)


#### 2014 {#2014}

-   [Cadth Recommendations As Predictors For Drug Availability In British Columbia And Ontario (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(14)00091-6/fulltext)


#### 2013 {#2013}

-   [HIV Regulatory Practices and Their Influence Over Reimbursement Decisions (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(13)02152-9/fulltext)
-   [A Case Study of FDA Practices and Its Influence on Regulatory and Reimbursement Decisions for Darunavir (conference abstract)](http://www.valueinhealthjournal.com/article/S1098-3015(13)02154-2/fulltext)


#### 2012 {#2012}

-   [The fine specificity of mannose-binding and galactose-binding lectins revealed using outlier motif analysis of glycan array data](https://www.ncbi.nlm.nih.gov/pubmed/?term=Daniel+Liden)


## Guinn Center for Policy Priorities Blogs {#guinn-center-for-policy-priorities-blogs}

I have worked at the Guinn Center for Policy Priorities, a nonprofit
policy research organization focused on policy issues impacting Nevada,
since September 2019. This is a selection of some blog posts I have
written in my capacity as director of data and analytics at the Guinn
Center.


### 2020 {#2020}

-   Blogs about the 2020 Census in Nevada:
    -   [Introduction](https://guinncenter.org/the-2020-census-in-nevada-snapshot-1/)
    -   [The Low Response Score](https://guinncenter.org/the-2020-census-in-nevada-snapshot-2/)
    -   [Low Response in Nevada's Cities](https://guinncenter.org/the-2020-census-in-nevada-snapshot-3/)
    -   [Nevada's Hard to Count Areas](https://guinncenter.org/the-2020-census-in-nevada-snapshot-4/)
    -   [Hard to Count Areas in Rural Nevada](https://guinncenter.org/the-2020-census-in-nevada-snapshot-5/)
    -   [Hard to Count Areas in Nevada's Cities](https://guinncenter.org/the-2020-census-in-nevada-snapshot-6/)
    -   [Language Proficiency and Census Response Rates](https://guinncenter.org/the-census-2020-in-nevada-snapshot-7/)
    -   [COVID-19 and the 2020 Census](https://guinncenter.org/the-census-2020-in-nevada-notice/)
    -   [Motivation for Completing the Census](https://guinncenter.org/the-2020-census-in-nevada-snapshot-8/)
    -   [The 2020 Census Response Rate Tracker](https://guinncenter.org/the-nevada-2020-census-response-rate-tracker/): I developed an R Shiny app that updated daily to show census response rates in Nevada (compared to other states), as well as in Nevada's counties and larger cities. Though the app itself is now offline (as the 2020 census has concluded), this post contains an overview of the project goals and some screenshots.
-   [Health Insurance Open Enrollment Numbers for Coverage Year 2020](https://guinncenter.org/health-insurance-open-enrollment-numbers-for-coverage-year-2020/)


### 2019 {#2019}

-   [Open Enrollment for Health Insurance in 2020: Big Changes and New Opportunities in Nevada](https://guinncenter.org/5693-2/)


## Context Matters Blogs {#context-matters-blogs}

I worked for Context Matters, a pharmaceutical data analytics and
technology company, from 2011 until mid-2019 and have written numerous
blogs about Health Technology Assessment (HTA) policy and analytics over
the years.

**NOTE** In 2020, old Context Matters blogs were removed from the website. Where possible, I updated the links to point to archived pages. Some, however, appear to be lost for good.

-   [Sovaldi and the Politics of High-Priced Drugs](http://web.archive.org/web/20170920032453/http://www.contextmatters.com/articles/sovaldi-and-the-politics-of-high-priced-drugs-are-we-asking-the-right-questions)
-   <span class="underline">The Real-World Consequences of Classification</span> (not archived)
-   <span class="underline">Opdivo: The High Stakes of Comparative Efficacy</span> (not archived)
-   [Big Changes to Cancer Drugs Fund as NICE Takes Over All UK Oncology Evaluations](https://web.archive.org/web/20160808050134/https://www.contextmatters.com/blognews/2016/5/5/big-changes-to-cancer-drugs-fund-as-nice-to-take-over-all-uk-oncology-evaluation)
-   [Science vs. Marketing](https://web.archive.org/web/20160815152418/https://www.contextmatters.com/blognews/2016/3/14/science-vs-marketing)
-   [A Case Study in Differing Approaches to Reimbursement Approval in Europe and the US](https://web.archive.org/web/20160412194448/https://www.contextmatters.com/blognews/2016/1/27/europe-us-casestudy)
-   <span class="underline">ASCO Conceptual Value Framework Demonstrates Gap in US Healthcare Valuation Process</span> (not archived)
-   <span class="underline">Paying for High-Priced Drugs: Comparing Global Strategies from U.S., UK, and Scotland</span> (not archived)
