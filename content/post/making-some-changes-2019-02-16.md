+++
title = "Making Some Changes"
date = 2019-02-19T08:23:00-08:00
tags = ["general"]
categories = ["Blog"]
draft = false
author = "Daniel Liden"
+++

## Switching to `Emacs Org mode` and `ox-hugo` {#switching-to-emacs-org-mode-and-ox-hugo}

I was previously using [blogdown](https://bookdown.org/yihui/blogdown/) to generate this static website from `.Rmd` documents. I was working almost exclusively with `R` at the time, and I didn't see that changing. Blogdown was a wonderful package to use to get this site up and running and it worked well for my first posts. However, my interests changed; I found it a little difficult to use outside of the context of `RStudio`; and it became a little harder to translate my current activities into `.Rmd` documents.

I also started moving a lot more of my general activities into `emacs org mode`. It's great for literate programming. It's excellent for keeping organized. It's fun to use. I like that my work, note-taking, code-writing, and organization can all take place under the same framework. I've only just scratched the surface of what `emacs` and `org mode` can do.

Anyway, to bring even more of my life under the `org mode` umbrella, I am now using the `ox-hugo` package to export posts written in `org mode` to my site. Further details on this excellent package can be found [here](https://ox-hugo.scripter.co/images/one-post-per-subtree.png).

Practically speaking, this shouldn't result in much of a visual difference (though I am considering some changes to the theme in the not-too-distant future).


## Content Changes {#content-changes}

I will be updating the "about" section to reflect this shortly. Some general notes about what will be changing:

-   I hope to post more. "Big projects" I'm working on will be split into smaller pieces so I don't feel the need to complete large-scale projects before posting. When I learn new things, I plan to post about them here. I will use these posts as a personal reference. I hope others also find them helpful.
-   I will not be posting notes or exercises from textbooks here (unless the problems are particularly interesting). I have started separate repos for textbook notes and exercises, which I will link to elsewhere. I will, however, periodically post updates about notes and exercises, with relevant links.
-   I will sometimes write about various aspects of my computing setup. I use gnu/linux and often need to reference various notes and sites on setup and use. Making my own references will be useful to me and, hopefully, to others with similar computing needs.

See the revised "about" page for more details.