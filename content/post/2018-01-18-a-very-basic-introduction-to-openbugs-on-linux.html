---
title: A (Very Basic) Introduction to OpenBUGS on Linux
author: Daniel Liden
date: '2018-01-18'
slug: a-very-basic-introduction-to-openbugs-on-linux
categories:
  - Blog
  - Instructional
tags:
  - R
  - blog
  - bayes
  - statistical inference
bibliography: bibliography1226.bib
---



<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>In this post, I outline the basics of setting up and using OpenBUGS on linux. <strong>BUGS</strong> stands for <strong>B</strong>ayesian inference <strong>U</strong>sing <strong>G</strong>ibbs <strong>S</strong>ampling. OpenBUGS allows for the analysis of highly-complex statistical models using Markov-chain Monte Carlo methods. It is specifically focused on Bayesian methods.</p>
<p>This guide may not be generalizable to all Linux systems, but it worked for me. It wasn’t too difficult, but I did have to pull together a number of different sources to get everything working as intended.</p>
<div id="installing-openbugs" class="section level2">
<h2>Installing OpenBUGS</h2>
<p>First of all, I am running Arch linux with kernel version <code>x86_64 Linux 4.14.13-1-ARCH</code>. I was not able to find any one-step installation package, so I followed the instructions on the <a href="http://www.openbugs.net/w/Downloads">OpenBUGS site</a>. The OpenBUGS site instructs the user to do the following:</p>
<p><strong>Download and Unpack the package</strong></p>
<pre class="bash"><code>tar zxvf OpenBUGS-3.2.3.tar.gz
cd OpenBUGS-3.2.3</code></pre>
<p><strong>Compile and Install</strong></p>
<pre class="bash"><code>./configure
make
sudo make install</code></pre>
<p>And, if your system has all of the necessary dependencies, that’s it! My system, however, did not have the necessary dependencies – there are some 32-bit C development packages required for installation. I installed the following packages in response to various error messages in the installation process: <code>lib32-glibc, lib32-gcc-libs, gcc6-libs, gcc6</code>. I’m not certain that all of those are strictly necessary, and I can’t guarantee others won’t be required: read the installation logs/error messages for details. After installing those packages, the rest of the installation went smoothly.</p>
</div>
<div id="making-openbugs-talk-to-r" class="section level2">
<h2>Making OpenBUGS talk to R</h2>
<p>I use R daily, so for me, it made sense to set up OpenBUGS to run within my existing R analysis/programming workflow. To that end, we install two packages, <code>R2OpenBUGS</code> and <code>coda</code>. I will explain what they do later. As with any other packages, install these in an R session with <code>install.packages(c(&quot;R2OpenBUGS&quot;,&quot;coda&quot;))</code>&quot;</p>
</div>
</div>
<div id="example" class="section level1">
<h1>Example</h1>
<p>I will replicate example 2.4 of <em>Bayesian Methods for Data Analysis</em> by Carlin and Louis <span class="citation">(Carlin and Louis 2008, 23)</span> using OpenBUGS within R. Note that this textbook uses WinBUGS, which is no longer under active development. Current development focuses on the open-source OpenBUGS, the subject of this guide.</p>
<p>In this example, we consider a normal (Gaussian) likelihood paramaterized by <span class="math inline">\(\theta\)</span> and <span class="math inline">\(\sigma^2\)</span> and a normal prior <span class="math inline">\(\pi(\theta | \eta)\)</span> parameterized by hyperparameters <span class="math inline">\(\eta = (\mu, \tau)\)</span>. Note that, in OpenBUGS, when specifying a normal distribution, we use precision = 1/variance instead of variance or SD as we might use in other settings – keep this in mind as we define our model.</p>
<p>OpenBUGS requires three main components to solve Bayesian modeling programs. We have to define (i) a statistical model; (ii) some data; and (iii) starting values to be used by the Markov-chain Monte Carlo sampling procedure. We follow the methods shown on the <a href="http://www.r-tutor.com/bayesian-statistics/openbugs">R tutorial website</a>.</p>
<div id="loading-required-packages" class="section level2">
<h2>Loading required packages</h2>
<p>As noted earlier, we require a couple of packages to carry out the analysis. They can be loaded as follows:</p>
<pre class="r"><code>library(&quot;R2OpenBUGS&quot;)
library(&quot;coda&quot;)
library(&quot;lattice&quot;)</code></pre>
</div>
<div id="defining-our-model" class="section level2">
<h2>Defining our model</h2>
<p>We define our model as an R function. We will never actually call this function, but this format allows the R2OpenBUGS package to properly invoke the OpenBUGS system. Note that the code within the function is taken directly from Carlin and Louis’s book. Note as well that, when defining variables in our model definition, we must use <code>&lt;-</code>; using an equals sign <code>=</code> will return an error. The tilde <code>~</code> means that a parameter follows a particular distribution.</p>
<p>Per the <code>bugs</code> (from the R2OpenBUGS package) function help documentation (viewed with <code>?bugs</code>), the model has to be saved as a <code>.txt</code> file. We save it to the <code>/tmp</code> directory using the <code>write.model</code> from the the <code>R2OpenBUGS</code> package.</p>
<pre class="r"><code>model=function(){
  prec.ybar&lt;-n/sigma2
  prec.theta&lt;-1/tau2
  ybar~dnorm(theta,prec.ybar)
  theta~dnorm(mu, prec.theta)
}

# write the model to a .txt file, save to /tmp
model.file=file.path(tempdir(),&quot;model.txt&quot;)
write.model(model,model.file)</code></pre>
</div>
<div id="defining-our-data-and-parameters-of-interest" class="section level2">
<h2>Defining our data and parameters of interest</h2>
<p>In this case, we only have one data point. We are treating this as a univariate problem, so we supply values for <span class="math inline">\(\mu\)</span>, <span class="math inline">\(\sigma^2\)</span>, and <span class="math inline">\(\tau^2\)</span>. The only parameter we want to perform inference on, in this case, is <span class="math inline">\(\theta\)</span>, so we must indicate this before running our BUGS process as well.</p>
<pre class="r"><code>data=list(ybar=6,mu=2,sigma2=1,tau2=1,n=1)

# parameter
params=c(&quot;theta&quot;)</code></pre>
</div>
<div id="defining-inital-values" class="section level2">
<h2>Defining inital values</h2>
<p>For the algorithm to work, we must supply plausible starting values for our parameters of interest. In this case, we are only interested in <span class="math inline">\(\theta\)</span>, so only need to supply a starting value for <span class="math inline">\(\theta\)</span>. As with the model, we also define the inital values as a function. Once again, R will not call this function, but defining it this way allows OpenBUGS to access it.</p>
<pre class="r"><code>inits=function() {list(theta=0)} </code></pre>
</div>
<div id="inference-with-bugs" class="section level2">
<h2>Inference with BUGS</h2>
<p>Now we call the <code>bugs()</code> function to run OpenBUGS from R. We pass the data, inital values, parameters, and model file as defined above.</p>
<pre class="r"><code>out=bugs(data,inits,params,model.file,n.iter=10000) </code></pre>
<p>From this, we can obtain (among other things) the mean and standard deviation of our parameter.</p>
<pre class="r"><code>out$mean[&quot;theta&quot;]</code></pre>
<pre><code>## $theta
## [1] 3.99604</code></pre>
<pre class="r"><code>out$sd[&quot;theta&quot;]</code></pre>
<pre><code>## $theta
## [1] 0.7107001</code></pre>
</div>
<div id="coda" class="section level2">
<h2>Coda</h2>
<p>Recall that, earlier, we installed and loaded the package <code>coda</code>. This package provides a set of tools for summarizing and visualizing results from Markov-chain Monte Carlo simulation experiments. It also has a suite of diagnostic tools, but we will not be focusing on those capabilities in this post. We will use the <code>coda</code> package to plot our distribution.</p>
<p>To use coda, we need to make a slight adjustment to our call to <code>bugs</code> – we need to add <code>codaPkg=TRUE</code> to the function call. Once that is done, we can easily see the density of our distribution.</p>
<pre class="r"><code>out=bugs(data,inits,params,model.file,codaPkg=TRUE, n.iter=10000)
coda.out=read.bugs(out)</code></pre>
<pre><code>## Abstracting deviance ... 5000 valid values
## Abstracting theta ... 5000 valid values
## Abstracting deviance ... 5000 valid values
## Abstracting theta ... 5000 valid values
## Abstracting deviance ... 5000 valid values
## Abstracting theta ... 5000 valid values</code></pre>
<pre class="r"><code>densityplot(coda.out)</code></pre>
<p><img src="/post/2018-01-18-a-very-basic-introduction-to-openbugs-on-linux_files/figure-html/coda-1.png" width="672" /></p>
<p>What we see resembles a normal density with a mean around 4. This makes sense given that the prior mean was 2 and the data sample mean was 6, and the variances of the two were the same.</p>
</div>
<div id="conclusion" class="section level2">
<h2>Conclusion</h2>
<p>This was a quick guide to get up and running with OpenBUGS and R on a Linux system. In future posts, we’ll delve into some more of the nuts and bolts of how to use OpenBUGS and how to interpret the results in light of general Bayesian data analysis principles and practices.</p>
<hr />
<div id="refs" class="references">
<div id="ref-carlin_bayesian_2008">
<p>Carlin, Bradley P., and Thomas A. Louis. 2008. <em>Bayesian Methods for Data Analysis, Third Edition</em>. CRC Press.</p>
</div>
</div>
</div>
</div>
