---
title: Visualizing linear programming problems II
author: Daniel Liden
date: '2018-02-21'
categories:
  - Notes & Exercises
tags:
  - books
  - linear programming
  - optimization
  - R
slug: visualizing-linear-programming-problems-ii
bibliography: bibliography1226.bib
---



<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>This is a followup to a <a href="http://www.danliden.com/post/2017/12/28/visualizing-linear-programming-problems/">prior post</a> on the same topic, where I used R’s base plotting system to visualize a linear programming provelm. I’m again looking at section 1.4 of <em>Introduction to Linear Optimization</em>. This time, we’ll consider Example 1.8 on page 23 <span class="citation">(Bertsimas and Tsitsiklis 1997, 23)</span>. The optimization problem in this example is:</p>
<p><span class="math display">\[\begin{align*}
-x_1+x_2 &amp;\leq 1 \\
x_1 &amp;\geq 0 \\
x_2 &amp;\geq 0
\end{align*}\]</span></p>
</div>
<div id="some-preliminary-notes" class="section level1">
<h1>Some preliminary notes</h1>
<ul>
<li>This time, instead of using the base R package, we’re going to plot the problem using the <code>ggplot2</code> package <span class="citation">(Wickham 2009)</span>.</li>
<li>This example is meant to illustrate that there may not be a unique optimal solution and that the feasible set will not necessarily be bounded. For example, given the cost function <span class="math inline">\(x_1+X_2\)</span>, which we can represent with the vector <span class="math inline">\(c=(1,1)\)</span>, the unique optimal solution is <span class="math inline">\(x=(0,0)\)</span>. However, given the cost function <span class="math inline">\(x_1\)</span> <span class="math inline">\((c=(1,0))\)</span>, we have infinite possible solutions. Setting <span class="math inline">\(x_1\)</span>=0, any choice of <span class="math inline">\(X_2 \leq 1\)</span> will be an optimal solution; that is, our solution has the form <span class="math inline">\(x=(0,x2)\)</span>.</li>
</ul>
<p>We can explore some of these possibilities with R’s optimization functions. We begin with the first example, with cost vector <span class="math inline">\(c=(1,1)\)</span>:</p>
<pre class="r"><code># Objective Function 1: x1+x2
obj.fun=function(x) x[1] + x[2]
# gradient
grad=function(x1,x2) c(1,1)
minim=constrOptim(theta=c(.01,.01),f=obj.fun,grad=grad,
                              ui=rbind(c(-1,1),c(1,0),c(0,1)),ci=c(-1,0,0))</code></pre>
<p>The unique minimizer of this cost function is 0, 0 and the minimum is, as expected, 0.</p>
<p>But what happens when we try this in a case without a single optimal solution? Now we will try the second case discussed above. The code is largely the same, but we change our objective function.</p>
<pre class="r"><code># Objective Function 1: x1+x2
obj.fun=function(x) x[1] + 0
# gradient
grad=function(x1,x2) c(1,0)
minim=constrOptim(theta=c(.01,.01),f=obj.fun,grad=grad,
                              ui=rbind(c(-1,1),c(1,0),c(0,1)),ci=c(-1,0,0))</code></pre>
<p>This returns 0, 0 as the minimizer and 0 as the minimum. In this case, <span class="math inline">\(X_2\)</span> remained practically unchanged from the initial value. So the function worked for identifying <em>an</em> optimal solution, but it is important to be remember that this is not a general solution and <span class="math inline">\(x_2\)</span> can take on any value in the feasible set.</p>
</div>
<div id="plotting-the-problem" class="section level1">
<h1>Plotting the problem</h1>
<p>Again, we are going to use ggplot2 to construct the plot this time. As in the first example, we will show <span class="math inline">\(x_1\)</span> on the horizontal axis and <span class="math inline">\(x_2\)</span> on the vertical. We follow <a href="http://rstudio-pubs-static.s3.amazonaws.com/3365_9573f6d661b444499365fe1841ee65d3.html">this guide</a> for some tips on how to plot functions with ggplot2.</p>
<div id="defining-the-function" class="section level2">
<h2>Defining the function</h2>
<p>Let’s think of <span class="math inline">\(x_2\)</span> as a function of <span class="math inline">\(X_1\)</span> for the sake of this visualization problem. We can represent <span class="math inline">\(-x_1+x_2 \leq 1\)</span> as <span class="math inline">\(x_2 \leq 1+x_1\)</span>. If we plot <span class="math inline">\(x_2=1+x_1\)</span>, everything under the curve will satisfy that constraint. To satisfy our other conditions, <span class="math inline">\(x_1\geq0\)</span> and <span class="math inline">\(x_2 \geq 0\)</span>, we will simply consider the area above the horizontal axis and to the right of the vertical axis.</p>
<pre class="r"><code>constraint=function(x1) 1+x1</code></pre>
</div>
<div id="plotting" class="section level2">
<h2>Plotting</h2>
<p>We use the <code>stat_function</code> argument to plot our constraint line (recall: a function of x1) and to fill in the area beneath it, representing the feasible set. The red lines show possible solutions.</p>
<pre class="r"><code>ggplot(data.frame(x=c(0, 2)), aes(x)) +
  stat_function(fun=constraint, geom=&quot;area&quot;,fill=&quot;gray76&quot;,alpha=0.5) +
  stat_function(fun=constraint, geom=&quot;line&quot;,col=&quot;blue&quot;,alpha=0.5) +
  geom_segment(mapping=aes(x=0,y=0,xend=2, yend=0), arrow=arrow(), size=0.5) +
  geom_segment(mapping=aes(x=0,y=x,xend=0, yend=3), arrow=arrow(), size=0.5) +
  ylab(&quot;x2&quot;) + xlab (&quot;x1&quot;) +
  ggtitle(&quot;Feasible Set for Optimization Problem&quot;) +
  ylim(-0.5,3) +
  geom_segment(mapping=aes(x=0,y=0,xend=2, yend=2),
               arrow=arrow(length = unit(0.1,&quot;inches&quot;)),
               size=0.5,col=&quot;red&quot;,alpha=0.4) +
  geom_text(aes(x=1,y=.8,label=&quot;c=(1,1)&quot;,col=&quot;red&quot;,alpha=0.4))</code></pre>
<p><img src="/post/2018-01-06-visualizing-linear-programming-problems-ii_files/figure-html/unnamed-chunk-1-1.png" width="672" />
Looking at this can tell us a lot about our possible solutions. It is clear, for instance, that the unique minimum for the cost function <span class="math inline">\(x_1+x_2\)</span> lies at <span class="math inline">\(x=0,0\)</span>: any other values of <span class="math inline">\(x_1\)</span> or <span class="math inline">\(x_2\)</span> in the feasible set will result in a larger cost. The line is plotted in red in the figure above. Similarly, if we consider <span class="math inline">\(x_1+0 \times x_2\)</span>, we can see that choosing <span class="math inline">\(x_1=0\)</span> and any <span class="math inline">\(x_2\)</span> value in the feasible set will return the (non-unique) minimum. For example, choosing <span class="math inline">\(x_1=0\)</span> and <span class="math inline">\(x_2 = 2\)</span> will give us <span class="math inline">\(0+0 \times 2=0\)</span>.</p>
<hr />
<div id="refs" class="references">
<div id="ref-bertsimas_introduction_1997">
<p>Bertsimas, Dimitris, and John N. Tsitsiklis. 1997. <em>Introduction to Linear Optimization</em>. Third printing edition. Belmont, Mass: Athena Scientific.</p>
</div>
<div id="ref-ggplot2">
<p>Wickham, Hadley. 2009. <em>Ggplot2: Elegant Graphics for Data Analysis</em>. Springer-Verlag New York. <a href="http://ggplot2.org" class="uri">http://ggplot2.org</a>.</p>
</div>
</div>
</div>
</div>
