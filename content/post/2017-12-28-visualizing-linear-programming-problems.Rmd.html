---
title: Visualizing linear programming problems
author: Daniel Liden
date: '2017-12-28'
categories:
  - Notes & Exercises
tags:
  - books
  - linear programming
  - optimization
  - R
slug: visualizing-linear-programming-problems
bibliography: bibliography1226.bib
---



<div id="introduction" class="section level1">
<h1>Introduction</h1>
<p>Today, I’m looking at section 1.4 of <em>Introduction to Linear Optimization</em>. The goal of this section is to find “useful geometric insights into the nature of linear optimization programming problems”. I will recreate the examples from the book in R.</p>
<p>In the following examples, we want to visually examine linear programming problems in order to:</p>
<ul>
<li>See what the objective function being optimized looks like</li>
<li>Visualize the feasible set based on the given constraints</li>
<li>If possible, visually identify solution.</li>
</ul>
<div id="example-1" class="section level2">
<h2>Example 1</h2>
<p>This is <strong>example 1.6</strong> on page 21 of <em>Introduction to Linear Optimization</em> <span class="citation">(Bertsimas and Tsitsiklis 1997, 27)</span>. Our aim is to minimize the function <span class="math inline">\(-x_1-x_2\)</span> subject to the following constraints:</p>
<p><span class="math display">\[\begin{align*}
x_1+2x_2 \leq 3 \\
2x_1+x_2 \leq 3 \\
x_1,\ x_2 \geq 0
\end{align*}\]</span></p>
</div>
<div id="visualizing-the-feasible-set" class="section level2">
<h2>Visualizing the feasible set</h2>
<p>First, we want to see what sorts of values the feasible set can take on. We begin by defining our objective function for later use:</p>
<pre class="r"><code>#Objective function
obj.fun=function(x) -x[1] - x[2]</code></pre>
<p>Next, we plot our constraints and determine which values of <span class="math inline">\(x_1\)</span> and <span class="math inline">\(x_2\)</span> fall into the feasible set. Note that both <span class="math inline">\(x_1\)</span> and <span class="math inline">\(x_2\)</span> are bounded above by <span class="math inline">\(3\)</span> and below by <span class="math inline">\(0\)</span> by the problem definition, so we restrict our attention to <span class="math inline">\(x = (x_1,x_2) \in [0,3] \times [0,3]\)</span>.</p>
<p>We will place <span class="math inline">\(x_1 \text{ and } x_2\)</span> on the x and y axes, respectively. We draw lines between the intercepts of the constraints by noting that <span class="math inline">\(x_1+2x_2=3\)</span> at points on the line connecting <span class="math inline">\((0,1.5)\)</span> and <span class="math inline">\((3,0)\)</span>. Likewise, we represent <span class="math inline">\(2x_1+x_2=3\)</span> by points on the line connecting <span class="math inline">\((1.5,0)\)</span> and <span class="math inline">\((0,3)\)</span>. Since <span class="math inline">\(x_1 \geq 0\)</span> and <span class="math inline">\(x_2 \geq 0\)</span>, the area between these lines and the axes represents the feasible set.</p>
<pre class="r"><code># setting up vertices for shading with polygon
x.vert=c(0,1.5,1,0)
y.vert = c(0,0,1,1.5)

plot(1,xlim=c(0,3),ylim=c(0,3),xlab=&quot;x1&quot;,ylab=&quot;x2&quot;,lty=2,lwd=1.5,
   main=&quot;feasible set for linear optimization problem&quot;)
lines(c(0,3),c(1.5,0),lwd=2,col=&quot;blue&quot;)
lines(c(0,1.5),c(3,0),lwd=2,col=&quot;blue&quot;)
polygon(x.vert,y.vert,col=rgb(1,0,0,0.5))
abline(0,-1,lty=4)
abline(1,-1,lty=4)
abline(2,-1,lty=4)
points(1,1,pch=19)
text(1.45,1,&quot;optimal point (1,1)&quot;)
text(0.5,0.5,&quot;feasible set&quot;)</code></pre>
<p><img src="/post/2017-12-28-visualizing-linear-programming-problems.Rmd_files/figure-html/feasible%20set-1.png" width="480" /></p>
<p>The red shaded region represents the feasible set. The blue lines represent the two constraint inequalities.</p>
<p>Note that we want to minimize <span class="math inline">\(-x_1-x_2\)</span> which is equivalent to maximizing <span class="math inline">\(x_1+x_2\)</span> within the given constraints. It is clear from the plot that the point <span class="math inline">\((1,1)\)</span> maximizes <span class="math inline">\(x_1+x_2\)</span> within the feasible set. Putting this into the original, we obtain <span class="math inline">\(-x_1-x_2 = -1-1 = -2\)</span>, so <span class="math inline">\(-2\)</span> is the minimum.</p>
<p>Plotting this was not entirely straightforward. The textbook image is easier to read:</p>
<div class="figure">
<img src="/images/img_122817.png" alt="textbook image" />
<p class="caption">textbook image</p>
</div>
<p>In particular, I liked that the lines extended past the axes, and I thought the lines were well-labeled. That was fairly difficult for me to do in the base R plotting system. I’m sure these things can be done in the basic R plotting package. I will tackle that in a future post.</p>
</div>
<div id="optimizing-with-rs-built-in-package" class="section level2">
<h2>Optimizing with R’s built-in package</h2>
<p>We conclude by quickly verifying the result we obtained through graphing by using R’s <code>constrOptim()</code> function.</p>
<p>Note that the gradient is <span class="math inline">\(\nabla f(x)= \begin{pmatrix}-1\\-1\end{pmatrix}\)</span>. We choose a starting value of <span class="math inline">\(\begin{pmatrix}0.1\\0.1\end{pmatrix}\)</span> which is in the feasible set. In the <code>constrOptim</code> function, the constraints are defined such that <code>ui %*% theta - ci &gt;= 0</code>. To put the constraints in this form, we define <span class="math inline">\(\textbf{ui}=\begin{pmatrix}-2 &amp; -1\\ -1 &amp; -2\end{pmatrix}\)</span> and <span class="math inline">\(\textbf{ci}=\begin{pmatrix}-3\\-3\end{pmatrix}\)</span>. We these arguments, we use the <code>constrOptim()</code> function like so:</p>
<pre class="r"><code>grad=function(x1,x2) c(-1,-1)
minim=constrOptim(theta=c(.01,.01),f=obj.fun,grad=grad,
            ui=rbind(-1*c(2,1),-1*c(1,2)),ci=-1*c(3,3))</code></pre>
<p>And we obtain the following values for the minimizer and minimum.</p>
<pre><code>## $par
## [1] 1 1</code></pre>
<pre><code>## $value
## [1] -2</code></pre>
<p>This corresponds to what we obtained by plotting the constraints. My next post on linear optimization will look at another example and will attempt to generate a more informative plot.</p>
<hr />
</div>
<div id="references" class="section level2 unnumbered">
<h2>References</h2>
<div id="refs" class="references">
<div id="ref-bertsimas_introduction_1997">
<p>Bertsimas, Dimitris, and John N. Tsitsiklis. 1997. <em>Introduction to Linear Optimization</em>. Third printing edition. Belmont, Mass: Athena Scientific.</p>
</div>
</div>
</div>
</div>
