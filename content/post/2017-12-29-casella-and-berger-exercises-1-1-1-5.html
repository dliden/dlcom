---
title: Casella and Berger exercises 1.1-1.5
author: Daniel Liden
date: '2017-12-29'
slug: casella-and-berger-exercises-1-1-1-5
categories:
  - Notes & Exercises
tags:
  - books
  - probability
  - R
  - statistical inference
  - casella and berger
bibliography: bibliography1226.bib
---



<div id="motivation" class="section level1">
<h1>Motivation</h1>
<p>I’m revisiting my old statistical inference textbook with the aim of slowly working through many of the problems. When I started out, Casella and Berger’s <em>Statistical Inference</em> was my first ever exposure to statistics, and I didn’t have the mathematical preparation for it. Since then, I’ve developed greater mathematical maturity and better general knowledge and intuition about statistics.</p>
<p>I also want to learn how to implement some of the procedures and verify some of the properties in the book using R. So, when relevant, I will include R code addressing some of the exercises.</p>
<p>I’m starting from the beginning. Some of the early exercises are not terribly interesting, but bear with me. We’ll get there in time.</p>
<div id="exercises" class="section level2">
<h2><strong>Exercises</strong></h2>
<p>We’re considering exercises 1-5, in section 1.7 <span class="citation">(Casella and Berger 2002, 37)</span>. This chapter is an introduction to probability and other preliminaries, such as basic set theory.</p>
<div id="exercise-1.1" class="section level3">
<h3><strong>Exercise 1.1</strong></h3>
<p><strong>For each of the following experiments, describe the sample space</strong></p>
<p>First, recall the definition of a sample space: The sample space is “The set, <span class="math inline">\(S\)</span>, of all possible outcomes of a particular experiment.” (p. 1).</p>
<div id="a-toss-a-coin-four-times." class="section level4">
<h4>(a) Toss a coin four times.</h4>
<p>The sample space can be represented by listing the possible outcomes: <span class="math inline">\(\{HHHH\}, \{HHHT\}, \{HHTH\}, \{HTHH\}, \\ \{THHH\}, \{HHTT\}, \{HTHT\}, \{HTTH\}, \\ \{THTH\}, \{TTHH\}, \{THHT\}, \{TTTH\}, \\ \{TTHT\}, \{THTT\}, \{HTTT\}, \{TTTT\}\)</span></p>
</div>
<div id="b-count-the-number-of-insect-damaged-leaves-on-a-plant" class="section level4">
<h4>(b) Count the number of insect-damaged leaves on a plant</h4>
<p>Since we are only interested in the number, and not in the extent of damage, the sample space is the set of nonnegative integers: <span class="math inline">\(S=\{0,1,2,3,...\}\)</span>. If we knew the total number of leaves (unlikely), we could put an upper bound on the possible outcomes.</p>
</div>
<div id="c-measure-the-lifetime-in-hours-of-a-particular-brand-of-light-bulb" class="section level4">
<h4>(c) Measure the lifetime (in hours) of a particular brand of light bulb</h4>
<p>The lifetime of a lightbulb can’t be negative, but there’s no reason the answer needs to be a whole number. There’s also no clear upper bound. So we can say <span class="math inline">\(S=[0,\infty]\)</span>.</p>
</div>
<div id="d-record-the-weights-of-10-day-old-rats" class="section level4">
<h4>(d) Record the weights of 10-day-old rats</h4>
<p>This is similar to the lighbulb problem. The weights don’t need to be in whole numbers. While there is no need to impose an upper bound, it is easier to find one here than it was in the lighbulb problem. We would be very surprised to find a 10-day-old rat weighing more than, say, 31 oz. So, measuring in ounces, we can say <span class="math inline">\(S=[0,32]\)</span> (though any upper bound will do)</p>
</div>
<div id="e-observe-the-proprtion-of-defectives-in-a-shipment-of-electronic-components." class="section level4">
<h4>(e) Observe the proprtion of defectives in a shipment of electronic components.</h4>
<p>The sample space here will take on rational number values. Suppose there are <span class="math inline">\(N\)</span> total components. Then the possible values for the proportion of defectives are <span class="math inline">\(S=\left\{\frac{1}{N}, \frac{2}{N},...,\frac{N}{N}\right\}\)</span>.</p>
</div>
</div>
<div id="exercise-1.2" class="section level3">
<h3><strong>Exercise 1.2</strong></h3>
<p><strong>Verify the Following Identities</strong></p>
<div id="a" class="section level4">
<h4>(a)</h4>
<p><span class="math inline">\(A \setminus B = A \setminus (A\cup B)=A\cap B^c\)</span></p>
<p><em>Proof</em> (i = iii):</p>
<p>Note that, to prove set equality, we prove (i) is a subset of (iii) and (iii) is a subset of (i).</p>
<p><span class="math display">\[\begin{align*}
x \in A \setminus B &amp;\Rightarrow x\in A \text{ and } x \notin B \\
&amp;\Rightarrow x \in A \text{ and } x \in B^c \\
&amp;\Rightarrow x \in A \cap B^c \\
&amp;\Rightarrow A \setminus B \subseteq A \cap B^c \\
\\
x \in A\cap B^c &amp;\Rightarrow x \in A \text{ and } x \in B^c \\
&amp;\Rightarrow x \in A \text{ and } x \notin B \\
&amp;\Rightarrow x \in A \setminus B \\
&amp;\Rightarrow A \cap B^c \subseteq A \setminus B \\
&amp;&amp;\blacksquare
\end{align*}\]</span></p>
<p>The proofs of the other identities follow from this because:</p>
<p><span class="math display">\[\begin{align*}
A \setminus (A \cap B) &amp;= A\cap (A \cap B)^c \\
&amp;=A \cap (A^c \cup B^c) &amp;&amp; \text{(DeMorgan&#39;s Laws)} \\
&amp;=(A \cap A^c) \cup (A \cap B^c) &amp;&amp; \text{(Distributive property)} \\
&amp;= \emptyset \cup (A \cap B^c) \\
&amp;= A\cap B^c = A \setminus B \\
&amp;&amp; \blacksquare
\end{align*} \]</span></p>
<p>We can verify some of this in R. We use the <code>setdiff()</code> and <code>intersect()</code> commands. We define a sample space <code>S</code> to contain <code>A</code> and <code>B</code>, so we can define <span class="math inline">\(B^c\)</span> as <span class="math inline">\(S\setminus B\)</span>.</p>
<pre class="r"><code># The Sample Space
S=1:20
# Subsets A and B, corresponding to problem
A=1:10
B=6:10
B_complement = setdiff(S,B)
A_complement = setdiff(S,A)

# Set Difference
setdiff(A,B)</code></pre>
<pre><code>## [1] 1 2 3 4 5</code></pre>
<pre class="r"><code># Set difference: A and A intersection B
setdiff(A, intersect(A,B))</code></pre>
<pre><code>## [1] 1 2 3 4 5</code></pre>
<pre class="r"><code># A intersection B complement
intersect(A,B_complement)</code></pre>
<pre><code>## [1] 1 2 3 4 5</code></pre>
<p>As you can see, we obtain the same answer from all three versions and, at least in this case, we have verified the identity.</p>
</div>
<div id="b" class="section level4">
<h4>(b)</h4>
<p><span class="math inline">\(B=(B \cap A) \cup (B \cap A^c)\)</span></p>
<p><em>Proof</em></p>
<p><em>Let <span class="math inline">\(S\)</span> represent the sample space</em></p>
<p><span class="math display">\[
\begin{align*}
B&amp;=(B \cap A) \cup (B \cap A^c) \\
&amp;= B \cap (A \cup A^c) &amp;&amp; \text{(Set distribution)} \\
&amp;= B \cap S &amp;&amp; (A \cup A^c =S) \\
&amp;= B &amp;&amp; (B \cap S = B) \\
\\
&amp;&amp; \blacksquare
\end{align*} \]</span></p>
<p>We again verify in R, with A, B, and S as defined above. We add the <code>union()</code> command to our repertoire.</p>
<pre class="r"><code>B</code></pre>
<pre><code>## [1]  6  7  8  9 10</code></pre>
<pre class="r"><code># (B intersect A) U (B intersect A complement)
union(intersect(A,B),intersect(A_complement,B))</code></pre>
<pre><code>## [1]  6  7  8  9 10</code></pre>
<p>Again, as expected, the identity holds in practice. We will dispense with the R examples in (c) and (d) as they are very similar.</p>
</div>
<div id="c" class="section level4">
<h4>(c)</h4>
<p>$ B A = B A^c$</p>
<p>This was proven in part (a).</p>
</div>
<div id="d" class="section level4">
<h4>(d)</h4>
<p><span class="math inline">\(A \cup B = A \cup (B \cap A^c)\)</span>. The proof is evident if we note, as in (b), that <span class="math inline">\(A \cup A^c = S\)</span>. Then <span class="math inline">\(A \cup (B \cap A^c) = (A \cup B) \cap (A \cup A^c) = (A \cup B) \cap S = A \cup B\)</span>.</p>
</div>
</div>
<div id="exercise-1.3" class="section level3">
<h3>Exercise 1.3</h3>
<p><strong>Show that:</strong></p>
<div id="a-commutativity-holds-for-intersection-and-union-of-sets-a-and-b" class="section level4">
<h4>(a) Commutativity holds for intersection and union of sets A and B</h4>
<p><span class="math display">\[ \begin{align*}
x \in A \cup B &amp;\Rightarrow x \in A \text{ or } x \in B \\
&amp;\Rightarrow x \in B \text{ or } x \in A \\
&amp;\Rightarrow x \in B \cup A \\
&amp;&amp; \blacksquare
\end{align*} \]</span></p>
<p>The argument for intersections is the same. We omit part (b) because the argument is essentially the same, with an extra step.</p>
</div>
<div id="c-prove-demorgans-laws-for-sets-a-and-b." class="section level4">
<h4>(c) Prove DeMorgan’s Laws for sets A and B.</h4>
<p>In this case, we will only prove DeMorgan’s Law for set intersections. The proof for unions is, again, very similar.</p>
<p><span class="math display">\[ \begin{align*}
x \in (A \cap B)^c &amp;\Rightarrow x \notin (A \cap B) \\
&amp;\Rightarrow x \notin A \text{ or } x \notin B \\
&amp;\Rightarrow x \in A^c \text{ or } x \in B^c  \\
&amp;\Rightarrow x \in A^c \cup B^c \\
&amp;\Rightarrow (A \cap B)^c \subseteq A^c \cup B^c \\
\\
x \in A^c \cup B^c &amp;\Rightarrow x \notin A \text{ or } x \notin B \\
&amp;\Rightarrow x \notin (A \cap B) \\
&amp;\Rightarrow x \in (A \cap B)^c \\
&amp;\Rightarrow A^c \cup B^c \subseteq (A \cap B)^c \\
&amp;&amp; \blacksquare
\end{align*} \]</span></p>
</div>
</div>
<div id="exercise-1.4-find-formulas-for-the-probabilities-of-the-following-events-in-terms-of-pa-pb-and-pa-intersection-b." class="section level3">
<h3>Exercise 1.4: Find formulas for the probabilities of the following events in terms of P(A), P(B), and P(A intersection B).</h3>
<div id="a-either-a-or-b-or-both" class="section level4">
<h4>(a) “Either A or B or both”</h4>
<p>This is just inclusive or, <span class="math inline">\(A \cup B\)</span>. However, we don’t have the union operation available. However, theorem 1.2.9 (p. 10) states that <span class="math inline">\(P(A\cup B) = P(A)+P(B)-P(A \cap B)\)</span>, which does exclusively use the tools available to us.</p>
</div>
<div id="b-either-a-or-b-but-not-both" class="section level4">
<h4>(b) “Either A or B but not both”</h4>
<p>This is <em>exclusive</em> or: we want the probability of <span class="math inline">\(A \cap B^c\)</span> or <span class="math inline">\(B \cap A^c\)</span>. In other words, we’re interested in P((A B^c)(B A^c)). Because <span class="math inline">\((A \cap B^c) \cup (B \cap A^c) = \emptyset\)</span>, we have <span class="math inline">\(P((A \cap B^c) \cup (B \cap A^c)) = P(A \cap B^c)+P(B \cap A^c) \\ = [P(A)-P(A \cap B)]+[P(B)-P(A \cap B)] \\ = P(A)+P(B)-2P(A \cap B)\)</span></p>
</div>
<div id="c-at-least-one-of-a-or-b" class="section level4">
<h4>(c) “At least one of A or B”</h4>
<p>This is the same as part (a).</p>
</div>
<div id="d-at-most-one-of-a-or-b" class="section level4">
<h4>(d) “At most one of A or B”</h4>
<p>It’s easiest to think about this problem in terms of what it excludes: <span class="math inline">\(A\cap B\)</span>. Everything else is included: Just <span class="math inline">\(A\)</span>, just <span class="math inline">\(B\)</span>, and neither <span class="math inline">\(A\)</span> nor <span class="math inline">\(B\)</span>. Recalling that the total probability is 1, we can represent the solution as <span class="math inline">\(1-P(A\cap B)\)</span>. This is the probability of the whole sample space minus the intersection of <span class="math inline">\(A \cap B\)</span>.</p>
</div>
</div>
<div id="exercise-1.5" class="section level3">
<h3>Exercise 1.5</h3>
<p><em>Approximately one-third of all human twins are identical (one-egg) and two-thirds are fraternal (two-egg) twins. Identical twins are necessarily the same sex, with male and female being equally likely. Among fraternal twins, approximately one-fourth are both female, one-fourth are both male, and half are one male and one female. Finally, among all U.S. births, approximately 1 in 90 is a twin birth. Define the following events:</em></p>
<ul>
<li><em>A = {a U.S. birth results in twin females}</em></li>
<li><em>B = {a U.S. birth results in identical twins}</em></li>
<li><em>C = {a U.S. birth results in twins}</em></li>
</ul>
<div id="part-a" class="section level4">
<h4>Part (a)</h4>
<p><em>State, in words, the event <span class="math inline">\(A \cap B \cap C\)</span></em></p>
<p>Event C represents twin females; B represents identical twins; and C represents twins in general. The intersection of these three, then, represents <strong>identical female twins</strong>.</p>
</div>
<div id="part-b" class="section level4">
<h4>Part (b)</h4>
<p><em>Find <span class="math inline">\(P(A\cap B\cap C)\)</span></em></p>
<p>We take <span class="math inline">\(P(C) \times P(B) \times P(A) = \frac{1}{90} \times \frac{1}{3} \times \frac{1}{2} = \frac{1}{540}\)</span>.</p>
</div>
</div>
</div>
<div id="concluding-notes" class="section level2">
<h2>Concluding Notes</h2>
<p>Again, these problems were pretty straightforward and mostly highlighted the basics of sets and probaiblity calculations. The problems will get more sophisticated as we get further along. Stay tuned! I will probably try to do one post like this each week.</p>
<hr />
<div id="refs" class="references">
<div id="ref-casella_statistical_2002">
<p>Casella, George, and Roger L. Berger. 2002. <em>Statistical Inference</em>. Thomson Learning.</p>
</div>
</div>
</div>
</div>
