+++
title = "New Data Science Portfolio"
date = 2019-03-05T15:14:00-08:00
tags = ["general", "blog"]
categories = ["blog"]
draft = false
author = "Daniel Liden"
+++

I added a new section to this website! It is a work in progress (currently a long wall of text), but it will serve as a portfolio of some of my data science and statistics projects. I will be updating it in the coming days and weeks with more code snippets and visualizations.

You can keep an eye on it [here](http://www.danliden.com/portfolio/). The link can also be found at the top of this page.
