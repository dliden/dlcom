+++
title = "Some Emacs Commands I Often Need to Look Up"
date = 2019-03-20T12:19:00-07:00
draft = false
author = "Daniel Liden"
+++

I do a lot of work in Emacs (mostly in org mode): I write posts for this site; I work through exercises in textbooks; I document personal projects; I take notes; I keep a journal; the list goes on. Most of the writing I do ends up in emacs at some time or another. There are plenty of commands I use all the time and that I don't need to look up anymore. I caught on to the basic navigation commands pretty quickly, and navigating -- at least within blocks of text -- has become muscle memory. But what about those less-frequently-used commands? The operations that are useful enough to look up but infrequent enough that they haven't yet stuck in my memory? Those are the subject of this post.


## Spellcheck {#spellcheck}

I like to spellcheck my drafts before sending them anywhere, and sometimes I just can't remember how to spell a certain word and I want confirmation without leaving my current window and checking online. The following commands are super helpful, assuming your system already has a spellchecking application installed (one of Hunspell, Aspell, Ispell or Enchant; I use aspell.).

-   `<M-$>`: Check/correct the spelling of the word at the current point.
-   `<M-x> Ispell`: check and correct the spelling of all words in the buffer or in the selected region.

Further details, particularly about how to navigate the interactive spellchecking process (e.g. marking words as correct, ignoring corrections, etc.) can be found [here](https://www.gnu.org/software/emacs/manual/html%5Fnode/emacs/Spelling.html).


## Deleting words {#deleting-words}

I often use the `delete` and `backspace` keys for any deletions of less than a line (and rely on `<C-k>` when I need to delete to the end of the line or delete full lines). But there are better ways to handle deletions of different sizes.

-   `<M-d>`: Delete (kill) from the current point to the next end of a word. So if the current point is in the middle of a word, this command will delete to the end of that word. If it is at the beginning of a word, the whole word will be deleted.
-   `<M-backspace>`: This behaves just like `<M-d>`, except that it goes from the current point to the _beginning_ of the word.

More details can be found [here](https://www.gnu.org/software/emacs/manual/html%5Fnode/emacs/Erasing.html).


## Navigating through a document {#navigating-through-a-document}

As I mentioned at the beginning, I quickly got the hang of navigating within relatively small blocks of text. `<C-a>` and `<C-e>` take care of moving to the beginning and end of the line. `<C-p>`, `<C-n>`, `<C-f>`, and `<C-b>` handle moving one line up or down or one space forward or back, respectively. `<M-f>` and `<M-b>` allow for moving forward and back by one _word_. But none of these are that helpful for navigating across much larger documents. I find myself occasionally needing to look up the commands for larger-scale navigation:

-   `<M-<>`: Navigate to the top of the current buffer.
-   `<M->>`: Navigate to the bottom of the current buffer.
-   `<C-v>`: Scroll the display one screen forward.
-   `<M-v>`: Scroll the display one screen back.
-   `<M-g> <M-g>`: Asks for a number and navigates to that line number.

More details can be found [here](https://www.gnu.org/software/emacs/manual/html%5Fnode/emacs/Moving-Point.html).

More posts like this will follow! There are many more Emacs commands I find myself looking up on a regular basis.