+++
title = "Embedding a Shiny App on a Website"
date = 2020-04-07T07:37:00-07:00
tags = ["reference", "blog", "R", "test", "instructional", "visualization"]
categories = ["blog"]
draft = false
author = "Daniel Liden"
+++

**Note 2:** Now that the 2020 Census has concluded, the embedded Shiny app has been shut down. I've left the code used to embed the app on the site, and I've linked to a blog post with screenshots of the app itself. But the app itself is no longer linked and no longer embedded here.
**Note:** This post was updated on 2020-07-10 with a new version of the Shiny app.

I recently developed an R Shiny app for tracking the daily 2020 census numbers in Nevada. It is hosted on <https://shinyapps.io>. But I wanted people to be able to view it here, on my website. The code used to embed it was:

`<iframe src="https://guinncenter.shinyapps.io/2020census/" width=1000 height=800></iframe>`

As noted above, now that the 2020 census has concluded, the app itself is no longer active. But a description and screenshots can be found [here](https://guinncenter.org/the-nevada-2020-census-response-rate-tracker/).
