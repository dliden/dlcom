+++
title = "Reference and Testing!"
tags = ["test", "reference"]
categories = ["blog"]
draft = true
author = "Daniel Liden"
+++

## Initial Notes {#initial-notes}

-   [this site](https://ox-hugo.scripter.co/) contains pretty much everything I need to work with.
-   I don't know how the `hugo` package works, but I should figure out the basics.
    -   use this: hugo server -D --navigateToChanged
-   The `CLOSED` flag, set up when changing the `TODO` flag to `DONE`, sets the date.
-   I'm guessing something about the formatting of "author" was carried over from my previous blogdown approach. It kept ending up in brackets. So the current setup with `hugo_custom_front_matter:` forces it into a string.
-   `hugo server -D --navigateToChanged`
-   run `hugo -D` to push changes
-   Changes to css fonts and styles should be made in `static/css`.


## Tags {#tags}

-   Tags are assinged in org mode with `<C-c><C-q>`
-   Categories are just tags preceded by `@`


## Math {#math}

`\(` and `\)` are better to use than `$ $` for inline. One-line uses `\[` and `\]`.

Test from the website: If \\(a^2=b\\) and \\( b=2 \\), then the solution must be either
\\[ a=+\sqrt{2} \\] or \\[ a=-\sqrt{2} \\]


## Source Blocks {#source-blocks}

I find these pretty ugly. Maybe I can change them. Still, I believe that is a problem for another time.

```python
return(1+1)
```

```org
2
```


## Figures {#figures}

Figures should be saved in static/...


## HTML not generated from org or RMD -- how to save "custom" files? {#html-not-generated-from-org-or-rmd-how-to-save-custom-files}

-   not sure about html
-   markdown can go in the content/ folder and will be generated as normal with `hugo -D`


## Embedding iframes {#embedding-iframes}

For some reason, the ox-hugo export is inserting "<" ">" around links in my iframe.

In the short term, to deal with this, export the hugo-compatible .md file; run "hugo" on command line; then go to public/post/ ... and find the html file for the post of interest and remove the <>.

Actually this isn't working. Need to update the _markdown_ file in content/post/...


## Still need to figure out: {#still-need-to-figure-out}

-   [ ] References
-   [ ] figures
    -   [ ] Esp. figures from code output
-   [ ] Would like to _not_ re-run sources code blocks each time.
