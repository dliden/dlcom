---
title: "About (about)"
date: 2021-03-26T15:26:00-07:00
draft: false
author: "Daniel Liden"
title: Daniel Liden
avatar: images/portrait.png
role: Data Scientist
bio: ""
organization:
  name: 
  url: 
social:
  - icon: github
    iconPack: fab
    url: https://github.com/djliden
  - icon: gitlab
    iconPack: fab
    url: https://gitlab.com/dliden/
  - icon: twitter
    iconPack: fab
    url: https://twitter.com/danjliden 
---

Most recently, Daniel Liden was the Director of Data Analytics at the Guinn Center for Policy
Priorities in Las Vegas, Nevada. In this role, he was responsible for conducting data-driven policy
analysis and for supporting ongoing research and data analysis at the Guinn Center.

Mr. Liden received his B.A. in the History of Science at the University of Chicago. Following this,
he worked for several years for Context Matters, Inc., a data analysis company focused on global
pharmaceutical reimbursement policies and data. While working at Context Matters, Mr. Liden wrote
extensively about drug pricing and reimbursement practices. He went on to receive an M.S. in
Statistics from the University of Minnesota, Twin Cities. His thesis work comprised a
simulation-based comparison of statistical tests for heterogeneity of risk difference between
patient subgroups in clinical trials.

In 2018, Mr. Liden was a graduate research fellow in the Data Science for the Public Good program at
the Biocomplexity Institute of Virginia Tech. Mr. Liden also worked on a variety of projects for
for-profit and nonprofit organizations as a statistics and data science consultant.


## Other Links {#other-links}

-   [twitter](https://twitter.com/danjliden)
-   my [gitlab](https://gitlab.com/dliden) repository (including source code for this site!)
