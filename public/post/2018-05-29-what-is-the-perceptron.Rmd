---
title: What is the Perceptron?
author: Daniel Liden
date: '2018-05-29'
slug: what-is-the-perceptron
categories:
  - Blog
tags:
  - blog
  - books
  - general
  - history
  - machine learning
---

As noted in the prior post, this is the first in a series of four (or so) posts about the perceptron learning algorithm. In this first post, we'll discuss what the perceptron is and how to implement it in `R`.

# What is the Perceptron?

Though there are many variations on the perceptron, we will be focusing on the simplest case: the perceptron learning algorithm is an approach to learning a binary classifier. A binary classifier is a function that distinguishes between two possible responses on the basis of the data. For example, a linear classifier could distinguish between two types of plants given characteristics such as flower color and height; between two types of cancer given gene profiles; or between different types of stars using light curve data.

The perceptron is a *linear* classifier. This means that it predicts classes using a linear combination of weights and data values. The simple, single-layer perceptron (we will learn what this means later) 

## Where does the term come from?

## 

# An Example in R


