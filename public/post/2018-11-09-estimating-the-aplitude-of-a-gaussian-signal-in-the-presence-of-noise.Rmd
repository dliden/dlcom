---
title: Estimating the Amplitude of a Gaussian Signal in the Presence of Noise
author: Daniel Liden
date: '2018-11-09'
slug: estimating-the-amplitude-of-a-gaussian-signal-in-the-presence-of-noise
categories:
  - Blog
  - Instructional
tags:
  - bayes
  - blog
  - books
  - optimization
  - probability
  - R
  - statistical inference
bibliography: bibliography1226.bib
---


# Introduction and Problem Setup

In this post, I'll be working through an example presented in Sivia and Skilling's *Data Analysis: A Bayesian Tutorial*. The problem, from chapter 3, involves estimation of two parameters: the amplitude and noise of a Gaussian signal @sivia_data_2006. The problem in this case is fairly straightforward: we assume the background $B$ is flat, and that the signal peak, with amplitude $A$, has known shape and location. The image below shows the problem setup.

![Problem Setup. Our goal is to estimate A and B.](/images/signalfig1.png)

A natural first question is: what data will we be using to estimate these parameters? In situations like this, we are often working with count data. The book mentions that these counts could represent, for instance, "the number of photons detected from a quasar at a certain wavelength or the number of neutrons scattered by a sample in a given direction." We will imagine an experimental setup wherein we have a detector with a set number of "channels." Each channel records the number of photons/neutrons/particles it detects. We will attempt to characterize the amplitude and background noise on the basis of these counts, and to describe the level of uncertainty associated with those estimates.

We will first simulate the data, and then use Bayesian methods to estimate the parameters.

First, we'll describe how we obtain our count data from a Gaussian signal.

# Data Generation

We don't have "real" experimental data to work with, so we will need to simulate some. This is a useful exercise even if we do have a source of real data: simulation forces us to check our understanding of the data-generating process assumed by our statistical procedures. In this case, we assume the "true signal" has a Gaussian shape. Let $x_0$ represent the location of the signal's peak; $x_k$ represent a channel on our signal detection apparatus; $w$ represent the width of the Gaussian peak; $A$ represent the amplitude, and $B$ represent the background. Then the "true signal" at location $x_k$ has the value $D_k$, defined as:

$$n_0\left[Ae^{(x_k-x_0)^2/2w^2}+B\right]$$.

$n_0$ is a scaling constant that might represent the amount of time over which measurements were obtained or the number of measurements taken. In this example, we will let $n_0 = \frac{100}{A+B}$, scaling the "true signal" such that the value of $D_k$ at $x_0$ always equals 100.

There are a couple of things to notice about $D_k$. First of all, it will not typically return integer values, so it doesn't correspond to our proposed experimental setup where we estimate our parameters on the basis of counts. Second, there is no random component, and thus nothing to estimate! That comes next.

If the true signal at location $x_k$ is $D_k$, we expect our sensor to observe a count $N_k$ in the nighborhood of $D_k$ at location $x_k$. In the case of counting experiments, we often turn to the Poisson distribution to characterize uncertainty or to generate data. In order to generate data for our experiment at each location $x_k$, we will make random draws from a Poisson distribution with expected value $D_k$. That is:

$$P(N_k|A, B, I) = \frac{D_k^{N_k}e^{-D_k}}{N_k!}$$.

The Poisson distribution is typically represented as $P(x|\lambda) = \frac{\lambda^xe^{-\lambda}}{x!}$. $\lambda$ is the expected value of the distribution. In our setup, $D_k$ takes the place of $\lambda$, so we expect a count in the vicinity of $D_k$.

Let's work though this in code. First we write a function to generate a "true" Gaussian signal and record values at a preset number of channels. We will assume our true amplitude $A$ is $4$ and our true background $B$ is $2$. We also take out width, $w$, to be $3$.

## True Gaussian Signal

```{r GaussSignal}
generate_gaussian_signal = function(nchannel, x0, A, B, w, max_signal){
  # nchannel is the number of channels
  # x_0 is the center of the Gaussian signal, or the location of the peak
  # A is the true amplitude
  # B is the true background
  # w is the width of the Gaussian signal
  # max_signal determines the scaling of the signal
  # Assume each channel has size 1
  xmin = x0 - nchannel/2 + 0.5
  xmax = x0 + nchannel/2 - 0.5
  x = seq(from = xmin, to = xmax, by = 1)
  D = A * exp(-(x-x0)^2/(2*w^2))+B
  D = D * (max_signal/(A + B))
  data.frame(x, D)
}
```

As the plot shows, we end up with a Gaussian signal with a peak value of 100 centered around $x_0=0$. The red line indicates the (scaled) background.

## Simulated Experimental Data

Now that we have our true signal, we can simulate our signal detection experiment. Suppose the above Gaussian curve represents nature, and we want to estimate that curve with a detector. Our signal detector will have fifteen channels, and we will record a count that might correspond to the number of photons observed over the course of the experiment in each of those channels. We expect the counts to be proportional to the true signal at each channel $x_k$. We will draw from a Poisson distribution with mean $D_k$ to simulate our experimental counts.

```{r expt}

# Function to generate counts

generate_experimental_counts = function(true_signal, seed = 145){
  # True signal obtained from generate_gaussian_signal function
  # seed is random seed
  
  set.seed(seed)
  channel = seq(from = -15, to = 15, by = 1)
  counts = numeric(31)
  for(i in 1:length(true_signal$x)){
    counts[i] = rpois(1, true_signal$D[i])
  }
  counts_out = data.frame(channel = channel, counts = counts)
  counts_out
}

```

We see that, as expected, our simulated experimental counts are close to the true signal values. What comes next? Now that we have generated a set of experimental results, we can "forget" about the true signal. Imagine now that all we have is the set of counts represented in the histogram above. From this set of counts, with no further knowledge about the true signal, we want to see how well we can estimate the amplitude $A$ and the background $B$. As noted above, we'll be using Bayesian methods to accomplish this.

We do not need to come up with a full equation for the posterior distribution: the values of A and B that maximize the nonnormalized posterior will be the same as those that maximize the normalized posterior. It is sufficient, therefore, to consider the product of the likelihood and prior distributions.

We are assuming that we know nothing about the true signal, so we will use a simple uniform prior:

$$
P(A,B|I) =
  \begin{cases}
    \text{constant} & \text{if A}\geq 0\ \text{ and  B } \geq 0 \\
    0 & \text{otherwise}
  \end{cases}       
$$

All this states is a belief that $A$ and $B$ must be greater than or equal to 0 (as, indeed, they must given the experiment). This is not the best prior to represent ignorance, but it works well enough for our purposes. The main point is that we are explicitly including our lack of prior knowledge in the model.

We've already discussed our likelihood function in the context of generating our experimental data. A few more details and we'll have all we need to conduct inference. As we noted above, the counts in each channel can be represented using a Poisson distribution.

$$P(N_k|A, B, I) = \frac{D_k^{N_k}e^{-D_k}}{N_k!}$$

To proceed from the counts *in each channel* to all of the counts in the experiment, we operate under the assumption that the counts are independent. That is, the counts obtained in one channel have no bearing on the counts obtained in another. We express this as follows:

$$P(\{N_k\}|A,B,I) = \Pi_{k=1}^M P(N_k|A,B,I)$$

With this, we have the tools to come up with our posterior distribution. We use Bayes' theorem:

$$P(A,B|\{N_k\},I) \propto P(\{N_k\}|A,B,I)\times P(A,B|I)$$

A simplifying note here is that the values of $A$ and $B$ maximizing the posterior will also maximize the log of the posterior. That will make our computations simpler as we will not need to deal with the product in the likelihood function. We will denote the log posterior by $L$:

$$L = \log[P(A,B|\{N_k\},I)] = \text{constant} + \sum_{k=1}^M[N_k\log(D_k) - d_k]$$

Where the constant includes terms not involving A and B. We now have the equation we want to maximize in order to come up with our Bayesian estimates of A and B. We take $n_0$ as given. As our final bit of preparation before conducting the simulation, we'll write code to evaluate the posterior probability.

This function takes as input our (simulated) dataset, pre-set parameters ($w$, $x_0$, $n_0$), a range of values we want to consider as candidates for $A$ and $B$, and the granularity with which we want to consider the values within those ranges (as represented by the number of steps between the minimum and maximum values for A and B). What this function ultimately gives us is a "grid" of log non-normalized posterior probability values corresponding to different values of $A$ and $B$. We can identify the "most likely" values of $A$ and $B$ by finding the highest value in the grid, and we can get a sense of the level of uncertainty around our estimates of $A$ and $B$ by looking at how close together or spread out the log non-normalized posterior probability values are.

```{r}
postMat = function(data, steps, x0, w, n0, arange, brange){
  Lmat = matrix(nrow = steps, ncol = steps)
  for(i in 1:length(arange)){
    for(j in 1:length(brange)){
      A = arange[i]
      B = brange[j]
      n0 = 10/(4+2)
      D = n0*(A*exp(-(data$channel-x0)^2/(2*w^2))+B)
      # Uniform Prior
      if (A >= 0 & B >= 0){
        Lmat[i,j] = log(n0) + sum(data$counts*log(D)-D)
      } else {
        Lmat[i, j] = 0
      }
    }
  }
  Lmat
}



gridOut = postMat(data = counts, steps = 100, x0 = 0, w = 1, arange = arange,
                  brange = brange)

contour(x = seq(from = 0, to = 6, by = (6/99)),
        y = seq(from = 0, to = 6, by = (6/99)), z = gridOut, nlevels = 10, zlim = c(50,80))

B = brange[36]
A = arange[73]

B

A

```


# Putting it all together

Following the book, we will consider four scenarios in our simulation study. In all cases, our "true values" are $A = 1$ and $B = 2$. We will consider:

* 15 bins; $n_0$ set such that the expected maximum count is 100; measurement from -7.5 to 7.5.
* 15 bins; $n_0$ set such that the expected maximum count is 10; measurement from -7.5 to 7.5.
* 31 bins; $n_0$ set such that the expected maximum count is 100; measurement from -15.5 to 15.5.
* 7 bins; $n_0$ set such that the expected maximum count is 100; measurement from -3.5 to 3.5.

We will discuss the differences between these scenarios as we proceed with the analysis.

## Generating "True" signals

We will generate the "true signal" twice, once with an expected maximum value of 100 and once with an expected maximum of 10. We will analyze different sets of "experimental data" corresponding to the above scenarios based on these "true signals".

```{r}

true_signal_100 = generate_gaussian_signal(nchannel = 31, x0 = 0, A = 1, B = 2,
                                       w = 2.13, max_signal = 100)
true_signal_10 = generate_gaussian_signal(nchannel = 31, x0 = 0, A = 1, B = 2,
                                          w = 2.13, max_signal = 10)


plot(true_signal_100$D~true_signal_100$x, type = "l", xlab = "x", ylab = "Signal",
     xlim = c(-15, 15), ylim = c(60, 100),
     main = c("True Signal; Expected Max Count = 100"))
abline(h = 2*(100/(2+1)), col = "red")
abline(h = 3*(100/(2+1)), col = "blue")
legend(x = 4, y = 95, fill = c("blue", "red"), legend = c("Expected Peak",
                                                          "Expected Background"))


plot(true_signal_10$D~true_signal_10$x, type = "l", xlab = "x", ylab = "Signal",
     xlim = c(-15, 15), ylim = c(6, 10),
     main = c("True Signal; Expected Max Count = 10"))
abline(h = 2*(10/(2+1)), col = "red")
abline(h = 3*(10/(2+1)), col = "blue")
legend(x = 4, y = 9.5, fill = c("blue", "red"), legend = c("Expected Peak",
                                                          "Expected Background"))
```

## Generating our "Experimental Data"

Now, we use our "true signals" to simulate experimental data that could result from an experiment wherein we make observations, in the form of counts, based on some device intended to measure the signal.

```{r}
exp100 = generate_experimental_counts(true_signal = true_signal_100, seed = 11)

exp10 = generate_experimental_counts(true_signal = true_signal_10, seed = 11)

# 100
counts100 = barplot(height = exp100$counts, axes = F, ylim = c(50,105),
                    xpd = F, width = 1, add = F)
axis(1,at=counts100,labels=exp100$channel)
axis(2,seq(50, 100, 10), c(50, 60, 70, 80, 90, 100))
lines(true_signal_100$D ~ counts100, col = "blue", par(lwd = 2))

# 10
counts10 = barplot(height = exp10$counts, axes = F, ylim = c(4,15),
                    xpd = F, add = F)
axis(1,at=counts10,labels=exp10$channel)
axis(2,seq(5, 10, 1), c(5, 6, 7, 8, 9, 10))
lines(true_signal_10$D ~ counts10, col = "blue", par(lwd = 2))


```
___
## References