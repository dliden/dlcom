---
title: Introduction
author: Daniel Liden
date: '2017-12-25'
slug: introduction
categories:
  - Blog
tags:
  - blog
  - general
---

Welcome to my site! I will be using this site for a few different purposes:

* Writing a blog
* Aggregating my other publications
* Posting notes and exercises from my statistics and math self-study
* Writing short reviews of the books I read

### blog

I used to write a lot more when I was a History of Science student at the University of Chicago. Studying statistics doesn't give me as many opportunities to write, but I think it's very important for people to write--and read--about statistics.

So much of the information we receive every day is conveyed in statistical terms, and it is the responsibility of those who work with statistics to communicate that information clearly. The blog component of this site will focus on just that--clear statistical communication. The topics will range from historical developments in statistics to discussions of new methods, but clarity will be the major focus of all posts.

### Other publications

I have written other things in the past, and I will be writing more things in the future. I want to keep them in one place. This will be that one place.

### Notes and Exercises

The transition from History of Science to Statistics wasn't straightforward. I spent a lot of time working though online courses and books to reach the point where pursuing a statistics MS was feasible. I still spend a lot of time on independent study. I'd like to have a place to keep notes and exercise solutions, both for my own reference and to help others working on similar material.

### Book Reviews

I read all the time. I want to be more deliberate about reflecting on the things I read for the sake of improving my recollection and deepening my understanding. To that end, I will write brief "reviews" or reflections about the books I read. These will not necessarily be related to statistics -- I read a lot of fiction.

## A note on R

This site is made using [blogdown](https://github.com/rstudio/blogdown), so I can write posts in Rmarkdown and show R code and output. I intent to make much use of this feature, especially in my self-study posts. Here are some examples:

### Basic Arithmetic

```{r}
1+1
3*3
```

### Simple plot

```{r,echo=FALSE}
a=1:10
b=1:10
plot(a,b)
abline(0,1)
```

Thank you for reading! Check back again soon for updates. I intend to post daily or as close to daily as I can manage.