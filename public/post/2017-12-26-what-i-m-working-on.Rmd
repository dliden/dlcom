---
title: What I'm working on
author: Daniel Liden
date: '2017-12-26'
slug: what-i-m-working-on
categories:
  - Blog
tags:
  - blog
  - general
bibliography: bibliography1226.bib
---

One of the first books I'll be working (partially) through is *Introduction to Linear Optimization* [@bertsimas_introduction_1997]. I recently took a statistical computing class that covered a selection of optimization topics. Though the course was far from comprehensive, it highlighted the value of having a range of optimization techniques, and a thorough grounding of how they work, in your toolbox. I will be working through the first four or five chapters of this book before moving onto nonlinear programming.

I'm also going to be revisiting *Statistical Inference* [@casella_statistical_2002]. That book was my first serious exposure to statistics, and I first approached it without much background in mathematics or statistics. My other coursework and self-study has gotten me to a point where I'm confident I can more productively work through the book.

For some more interesting posts, I will select some of the datasets from the `histdata` R package and provide some historical background in addition to brief data analyses. I'm particularly interested in recreating the methods originally used to analyze the data and comparing those to contemporary approaches.

Unrelated to statistics, I'm currently reading Roberto Bolaño's *2666* [@bolano_2666:_2013] and will compose a short writeup about the book once I finish it.

___

